
# https://people.cs.pitt.edu/~jmisurda/teaching/cs447/examples/Booth%20Example.pdf
# https://www.studytonight.com/computer-architecture/booth-multiplication-algorithm
# https://www.geeksforgeeks.org/computer-organization-booths-algorithm/ 

# Aloca um registrador na pilha
.macro push (%reg)
    addiu $sp, $sp, -4
    sw    %reg, 0($sp)
.end_macro

# Restaura um registrador da pilha
.macro pull (%reg)
    lw    %reg, 0($sp)
    addiu $sp, $sp, 4
.end_macro

# Sai do programa com o código
.macro quit(%exitCode)
    li   $v0, 17
    li   $a0, %exitCode
    syscall	
.end_macro

# Lê um inteiro
.macro readInt(%reg)
    push ($v0)
    li   $v0, 5
    syscall	
    move %reg, $v0
    pull ($v0)
.end_macro

# Imprime um inteiro em forma decimal
.macro printIntDec(%reg)
    push ($v0) 
    push ($a0)
    move $a0, %reg
    li   $v0, 1
    syscall	
    pull ($a0)
    pull ($v0) 
    printString("\n")
.end_macro

# Imprime um inteiro em forma hexadecimal
.macro printIntHex(%reg)
    push ($v0) 
    push ($a0)
    move $a0, %reg
    li   $v0, 34
    syscall	
    pull ($a0)
    pull ($v0)
    printString("\n")
.end_macro

# Imprime um inteiro em forma binária
.macro printIntBin(%reg)
    push ($v0) 
    push ($a0)
    move $a0, %reg
    li   $v0, 35
    syscall	
    pull ($a0)
    pull ($v0) 
    printString("\n")
.end_macro

# Imprime uma string
.macro printString(%str)
    .data
    str: .asciiz %str
    .text
    push ($v0) 
    push ($a0)
    li   $v0, 4
    la   $a0, str
    syscall
    pull ($a0)
    pull ($v0)
.end_macro

.text
main:
    # Lê os valores para multiplicar
    printString("Insira o multiplicando: ")
    readInt($a0)
    printString("Insira o multiplicador: ")
    readInt($a1)

    # Imprime o valor usando a instrução mul
    # Somente para fins de comparação
    printString("\nUsando mul: \n")
    mul $v0, $a0, $a1
    printIntDec($v0)
    printIntHex($v0) 

    # Chama o procedimento que multiplica os valores
    jal multiply

    # Imprime o resultado
    printString("\nUsando algoritmo de booth: \n")
    printIntDec($v0)
    printIntHex($v0)
    printIntHex($v1)
    
    # Termina a execução do programa
    quit(0)

# Argumentos
# $a0 = M = Multiplicando
# $a1 = Q = Multiplicador
# Retornos
# $v0 = Produto LO
# $v1 = Produto HI
multiply:
    move $v0, $a1 # Move nosso multiplicador para os bits menos significativos
    li   $v1, 0 # Inicia os bits mais significativos com 0
    li   $t1, 0 # Registrador que vai guardar nosso bit menos significativo
    li   $t2, 0 # Registrador que vai guardar o bit menos significativo anterior, inicia com 0
    li   $t3, 0 # Indice do loop, o algoritmo é executado 32 vezes
    
    multiply_loop: 
        and $t1, $v0, 1 # Copia o bit menos significativo

        # Escolhe a operação para executar comparando o bit menos
        # significativo com o anterior
        beq $t1, $t2 multiply_switch_break # 11 || 00
        bgt $t1, $t2 case_0                # 10
        blt $t1, $t2 case_1                # 01

        case_0: # Se for 10, subtraimos o multiplicando dos bits mais significativos do resultado
            subu $v1, $v1, $a0 
        j multiply_switch_break
        
        case_1: # Se for 01, adicionamos o multiplicando aos bits mais significativos do resultado
            addu $v1, $v1, $a0 
        j multiply_switch_break
        
        # Caso seja 00 ou 11 nenhuma operação é realizada
        multiply_switch_break:
        
        # Move o resultado para a direita aritméticamente
        # Respeitando o limite entre os dois registradores
        # E copiando o bit menos significativo do HI para o mais significativo de LO
        and   $t4, $v1, 1 # LSB HI
        sll   $t4, $t4, 31
        srl   $v0, $v0, 1
        or    $v0, $v0, $t4

        sra   $v1, $v1, 1

        move  $t2, $t1 # Copia o bit menos significativo para ser usado na proxima iteração
        addiu $t3, $t3, 1 # Incrementa o indice do loop

    bne  $t3, 32, multiply_loop # Executa o loop 32 vezes
    jr   $ra # Retorna do procedimento