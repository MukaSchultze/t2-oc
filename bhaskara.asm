
# Aloca um registrador na pilha
.macro push (%reg)
    addiu $sp, $sp, -4
    sw    %reg, 0($sp)
.end_macro

# Restaura um registrador da pilha
.macro pull (%reg)
    lw    %reg, 0($sp)
    addiu $sp, $sp, 4
.end_macro

# Aloca um registrador na pilha
.macro pushDouble (%reg)
    addiu $sp, $sp, -8
    sdc1  %reg, 0($sp)
.end_macro

# Restaura um registrador da pilha
.macro pullDouble (%reg)
    ldc1  %reg, 0($sp)
    addiu $sp, $sp, 8
.end_macro

# Sai do programa com o código
.macro quit(%exitCode)
    li   $v0, 17
    li   $a0, %exitCode
    syscall	
.end_macro

# Lê uma double
.macro readDouble(%reg)
    push ($v0)
    pushDouble ($f0) 
    li    $v0, 7
    syscall 
    mov.d %reg, $f0
    pullDouble ($f0)
    pull ($v0)
.end_macro

# Imprime uma double em forma decimal
.macro printDouble(%reg)
    push ($v0) 
    pushDouble ($f12) 
    mov.d $f12, %reg
    li   $v0, 3
    syscall	 
    pullDouble ($f12)
    pull ($v0) 
.end_macro

# Imprime uma string
.macro printString(%str)
    .data
    str: .asciiz %str
    .text
    push ($v0) 
    push ($a0)
    li   $v0, 4
    la   $a0, str
    syscall
    pull ($a0)
    pull ($v0)
.end_macro

.text
main:
    # Lê as váriaveis de entrada do programa
    printString("A: ")
    readDouble($f2)
    printString("B: ")
    readDouble($f4)
    printString("C: ")
    readDouble($f6)

    # Executa o procedimento de cálculo das raizes
    jal bhaskara
    
    # Imprime as duas raizes da equação
    printString("\nX': ") 
    printDouble($f8)
    
    printString("\nX\": ") 
    printDouble($f10)

    # Encerra a execução do programa
    quit(0)

# Argumentos
# $f2 = A
# $f4 = B
# $f6 = C
# Retornos
# $f8 = X'
# $f10 = X"
bhaskara:
    # (-B +- sqrt(B*B - 4*A*C)) / 2A
    push($ra) # Armazena o endereço de retorno
    jal delta # Calcula o delta da equação
    mov.d  $f12, $f8 # Move o retorno do delta para $f12
    l.d    $f8, number0 # Carrega numero 0 para comparação
    c.lt.d $f12, $f8 # Compara se delta < 0
    bc1f positiveDelta # Se delta > 0, temos raizes
    l.d    $f8, number0 # Reseta os valores de retorno
    l.d    $f10, number0
    printString("\nNão há raizes reais\n")
    pull($ra) # Restaura o endereço de retorno
    jr $ra # Retorna a função
    positiveDelta: # Caso o delta seja positivo
    sqrt.d $f12, $f12 # Aplica a raiz quadrada do delta
    l.d    $f14, number2 # Carrega o número 2 para uso posterior
    mul.d  $f14, $f14, $f2 # 2 * A
    l.d    $f16, numberN1 # Carrega o número -1 para uso posterior
    mul.d  $f6, $f16, $f6 # B * -1 = -B
    add.d  $f8, $f6, $f12 # -B + sqrt(delta)
    div.d  $f8, $f8, $f14 # -B + sqrt(delta) / 2A
    sub.d  $f10, $f6, $f12 # -B - sqrt(delta)
    div.d  $f10, $f10, $f14 # -B - sqrt(delta) / 2A
    c.lt.d $f8, $f12 # Compara se x' < x"
    bc1t end # Caso as raizes estejam ordenadas pula para final
    mov.d  $f10, $f12 # Ordena os registradores em ordem crescente
    mov.d  $f12, $f8
    mov.d  $f8, $f10
    end:
    pull($ra) # Restaura o endereço de retorno
    jr $ra # Retorna a função

# Argumentos
# $f2 = A
# $f4 = B
# $f6 = C
# Retorno
# $f8 = Delta
delta: # Calculo do delta da função de segundo grau
    pushDouble($f10) # Salva o registrador na pilha
    l.d   $f10, number4 # Carrega o valor 4 para uso posterior
    mul.d $f10, $f10, $f2 # 4*A
    mul.d $f10, $f10, $f6 # 4*A*C
    mul.d $f8, $f4, $f4 # B*B
    sub.d $f8, $f8, $f10 # B*B - 4*A*C
    pullDouble($f10) # Recupera o registrador da pilha
    jr    $ra # Retorna do procedimento

.data
    # Constantes em ponto flutuante de precisão dupla
    numberN1: .double -1 
    number0: .double 0
    number2: .double 2
    number4: .double 4
